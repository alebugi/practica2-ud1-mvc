package com.alexbg.balonmanomvc.base;

import java.time.LocalDate;

public class Escolar extends Jugador{
    public String categoria;

    public Escolar(){
        super();
        this.categoria = "";
    }

    public Escolar(String nombreJugador, String club, String posicion, double peso, double altura, LocalDate fechaAlta, String categoria) {
        super(nombreJugador, club, posicion, peso, altura, fechaAlta);
        this.categoria = categoria;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return "Escolar{" +
                "nombreJugador='" + nombreJugador + '\'' +
                ", club='" + club + '\'' +
                ", posicion='" + posicion + '\'' +
                ", peso='" + peso + '\'' +
                ", altura=" + altura +
                ", fechaAlta=" + fechaAlta +
                ", categoria=" + categoria +
                '}';
    }
}
